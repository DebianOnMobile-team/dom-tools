# DebianOnMobile Tools

Tools to help with packaging components for

    https://salsa.debian.org/DebianOnMobile-team

# Documentation

To build the manpage just invoke

    sudo apt -y install perl make shellcheck
    make

