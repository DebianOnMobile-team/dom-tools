SHELL_SCRIPTS=\
  release-dom-component \
  $(NULL)

PYTHON_SCRIPTS=\
  poll-gitlab-mr
  $(NULL)

MANPAGES=\
  release-dom-component.1 \
  $(NULL)

all: $(MANPAGES)
	shellcheck ${SHELL_SCRIPTS}
	flake8 ${PYTHON_SCRIPTS}

%.1: %
	pod2man --center "" --release "" --section=1 --utf8 $< $@

clean:
	rm *.stamp
