# DebianOnMobile maintainers packaging policy

Please follow the following patterns:

Git Repository layout
=====================

* Follow [DEP-14](https://dep-team.pages.debian.net/deps/dep14/)
* Name the packaging branch `debian/latest` and develop new releases
  there.
* Name the upstream branch `upstream/latest` and develop new releases
  there.

Git-buildpackage
================
* If packaging with git-buildpackage please use a `debian/gbp.conf` like

```ini
[DEFAULT]
# Repo layout as described above:
debian-branch = debian/latest
debian-tag = debian/%(version)s
upstream-branch = upstream/latest
upstream-tag = upstream/%(version)s
# Track upstream VCS:
upstream-vcs-tag=v%(version)s
pristine-tar = True

[tag]
# Sign tags by default
sign-tags = true

[import-orig]
# Bump version after import
postimport = dch -v%(version)s New upstream release; git add debian/changelog; debcommit
upstream-vcs-tag = v%(version%~%_)s

[dch]
multimaint-merge = True
```
* Maintain `debian/upstream/metadata` so `gbp clone --add-upstream-vcs` can work
